# Specifications

## System Requirements

Tested on WSL2 (Windows Subsystem for Linux) with the following Linux distribution:

- **Debian GNU/Linux 11 (bullseye)** on Windows 10 x86_64

# Using _Be Box Office Planet_

## Launching / Stopping the application

### Launch application via **startup.sh**

    ./startup.sh

Quick access to your deployments:

- [Dozzle Logger](http://localhost:8081)
- [Be-Box-Office-Planet](http://localhost:8080/Bebop-Web)

### Stop application via **teardown.sh**

    ./teardown.sh
