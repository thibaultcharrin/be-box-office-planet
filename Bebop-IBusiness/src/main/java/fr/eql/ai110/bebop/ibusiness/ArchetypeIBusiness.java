package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.ArchetypeEntity;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface ArchetypeIBusiness {
	
	//CREATE
	/** creates single archetype entity */
	boolean createArchetypeEntity(ArchetypeEntity archetypeEntity);
	
	//READ
	/** returns list of archetype entities */
	List<ArchetypeEntity> findAllArchetypeEntities();
	
	/** returns list of archetype entities filtering by name */
	List<ArchetypeEntity> findArchetypeEntitiesByName(String name);
	
	/** returns single archetype entity using id */
	ArchetypeEntity findArchetypeEntityById(Integer id);
	
	/** returns number of archetype entities */
	Long findNbArchetypes();
	
	//UPDATE
	/** updates single archetype entity **/
	boolean updateArchetypeEntity(ArchetypeEntity archetypeEntity);
	
	//DELETE
	/** removes single archetype entity **/
	boolean deleteArchetypeEntity(ArchetypeEntity archetypeEntity);
}
