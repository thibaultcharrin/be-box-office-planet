package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Venue;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface VenueIBusiness {
	
	//CREATE
	/** creates single venue */
	boolean createVenue(Venue venue);
	
	//READ
	/** returns list of venues */
	List<Venue> findAllVenues();
	
	/** returns list of venues filtering by name */
	List<Venue> findVenuesByName(String name);
	
	/** returns single venue using id */
	Venue findVenueById(Integer id);
	
	/** returns number of venues */
	Long findNbVenues();
	
	//UPDATE
	/** updates single venue **/
	boolean updateVenue(Venue venue);
	
	//DELETE
	/** removes single venue **/
	boolean deleteVenue(Venue venue);
}
