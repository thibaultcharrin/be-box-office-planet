package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Schedule;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface ScheduleIBusiness {
	
	//CREATE
	/** creates single schedule */
	boolean createSchedule(Schedule schedule);
	
	//READ
	/** returns list of schedule */
	List<Schedule> findAllSchedules();
	
	/** returns single schedule using id */
	Schedule findScheduleById(Integer id);
	
	/** returns number of schedules */
	Long findNbSchedules();
	
	//UPDATE
	/** updates single schedule **/
	boolean updateSchedule(Schedule schedule);
	
	//DELETE
	/** removes single schedule **/
	boolean deleteSchedule(Schedule schedule);
}
