package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.City;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface CityIBusiness {
	
	//CREATE
	/** creates single city */
	boolean createCity(City city);
	
	//READ
	/** returns list of cities */
	List<City> findAllCities();
	
	/** returns list of cities filtering by name */
	List<City> findCitiesByName(String name);
	
	/** returns single cities using id */
	City findCityById(Integer id);
	
	/** returns number of cities */
	Long findNbCities();
	
	//UPDATE
	/** updates single city **/
	boolean updateCity(City city);
	
	//DELETE
	/** removes single city **/
	boolean deleteCity(City city);
}
