package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Reward;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface RewardIBusiness {
	
	//CREATE
	/** creates single reward */
	boolean createReward(Reward reward);
	
	//READ
	/** returns list of rewards */
	List<Reward> findAllRewards();
	
	/** returns list of rewards filtering by name */
	List<Reward> findRewardsByName(String name);
	
	/** returns single reward using id */
	Reward findRewardById(Integer id);
	
	/** returns number of rewards */
	Long findNbRewards();
	
	//UPDATE
	/** updates single reward **/
	boolean updateReward(Reward reward);
	
	//DELETE
	/** removes single reward **/
	boolean deleteReward(Reward reward);
}
