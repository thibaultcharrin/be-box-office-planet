package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Event;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface EventIBusiness {
	
	//CREATE
	/** creates single event */
	boolean createEvent(Event event);
	
	//READ
	/** returns list of events */
	List<Event> findAllEvents();
	
	/** returns list of events filtering by name */
	List<Event> findEventsByName(String name);
	
	/** returns single event using id */
	Event findEventById(Integer id);
	
	/** returns number of events */
	Long findNbEvents();
	
	//UPDATE
	/** updates single event **/
	boolean updateEvent(Event event);
	
	//DELETE
	/** removes single event **/
	boolean deleteEvent(Event event);
}
