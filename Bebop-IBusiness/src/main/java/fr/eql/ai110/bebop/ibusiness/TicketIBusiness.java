package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Ticket;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface TicketIBusiness {
	
	//CREATE
	/** creates single ticket */
	boolean createTicket(Ticket ticket);
	
	//READ
	/** returns list of tickets */
	List<Ticket> findAllTickets();
	
	/** returns list of ticket filtering by name */
	List<Ticket> findTicketsByName(String name);
	
	/** returns single ticket using id */
	Ticket findTicketById(Integer id);
	
	/** returns number of tickets */
	Long findNbTickets();
	
	//UPDATE
	/** updates single ticket **/
	boolean updateTicket(Ticket ticket);
	
	//DELETE
	/** removes single ticket **/
	boolean deleteTicket(Ticket ticket);
}
