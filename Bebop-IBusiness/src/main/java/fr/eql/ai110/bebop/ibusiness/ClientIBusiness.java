package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Admin;
import fr.eql.ai110.bebop.entity.Client;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface ClientIBusiness {
	
	//AUTHENTICATE
	/** hashes password **/
	String hashPassword(String password);
	
	/** connects clients **/
	Client connectClient(Client client);
	
	//CREATE
	/** creates single client */
	boolean createClient(Client client);
	
	//READ
	/** returns list of clients */
	List<Client> findAllClients();
	
	/** returns list of clients by multicriteria **/
	List<Client> findClientsByMulti(Client client);
	
	/** returns list of clients filtering by name */
	List<Client> findClientsByName(String name);
	
	/** returns single client using id */
	Client findClientById(Integer id);
	
	/** returns number of clients */
	Long findNbClients();
	
	//UPDATE
	/** updates single client **/
	boolean updateClient(Client client);
	
	//DELETE
	/** removes single client **/
	boolean deleteClient(Client client);
}
