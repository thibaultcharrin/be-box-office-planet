package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Category;
import fr.eql.ai110.bebop.idao.CategoryIDao;


@Remote(CategoryIDao.class)
@Stateless
public class CategoryDao extends GenericDao<Category> implements CategoryIDao {

	@Override
	public Long countCategories() {
		Query query = em.createQuery("SELECT COUNT(c) FROM Category c");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Category> c;
		Query query = em.createQuery("SELECT c FROM Category c WHERE c.name= :nameParam");
		query.setParameter("nameParam", name);
		c = query.getResultList();
		if (c.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Category> getCategoriesByName(String name) {
		Query query = em.createQuery("SELECT c FROM Category c WHERE c.name LIKE :nameParam ");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Category>)query.getResultList();
	}
}
