package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Country;
import fr.eql.ai110.bebop.idao.CountryIDao;


@Remote(CountryIDao.class)
@Stateless
public class CountryDao extends GenericDao<Country> implements CountryIDao {

	@Override
	public Long countCountries() {
		Query query = em.createQuery("SELECT COUNT(c) FROM Country c");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Country> c;
		Query query = em.createQuery("SELECT c FROM Country c WHERE c.name= :nameParam");
		query.setParameter("nameParam", name);
		c = query.getResultList();
		if (c.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Country> getCountriesByName(String name) {
		Query query = em.createQuery("SELECT c FROM Country c WHERE c.name LIKE :nameParam ");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Country>)query.getResultList();
	}
}
