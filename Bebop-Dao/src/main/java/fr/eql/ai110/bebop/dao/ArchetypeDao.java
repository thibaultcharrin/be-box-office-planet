package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.ArchetypeEntity;
import fr.eql.ai110.bebop.idao.ArchetypeIDao;

@Remote(ArchetypeIDao.class)
@Stateless
public class ArchetypeDao extends GenericDao<ArchetypeEntity> implements ArchetypeIDao {

	@Override
	public Long countArchetypeEntities() {
		Query query = em.createQuery("SELECT COUNT(a) FROM ArchetypeEntity a");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<ArchetypeEntity> ae;
		Query query = em.createQuery("SELECT a FROM ArchetypeEntity a WHERE a.name= :nameParam");
		query.setParameter("nameParam", name);
		ae = query.getResultList();
		if (ae.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<ArchetypeEntity> getArchetypeEntitiesByName(String name) {
		Query query = em.createQuery("SELECT a FROM ArchetypeEntity a WHERE a.name LIKE :nameParam ");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<ArchetypeEntity>)query.getResultList();
	}
}
