package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Reward;
import fr.eql.ai110.bebop.idao.RewardIDao;


@Remote(RewardIDao.class)
@Stateless
public class RewardDao extends GenericDao<Reward> implements RewardIDao {

	@Override
	public Long countRewards() {
		Query query = em.createQuery("SELECT COUNT(r) FROM Reward r");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Reward> r;
		Query query = em.createQuery("SELECT r FROM Reward r WHERE r.redeemCode= :nameParam");
		query.setParameter("nameParam", name);
		r = query.getResultList();
		if (r.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Reward> getRewardsByName(String name) {
		Query query = em.createQuery("SELECT r FROM Reward r WHERE r.redeemCode LIKE :nameParam ORDER BY r.redeemCode ASC");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Reward>)query.getResultList();
	}
}
