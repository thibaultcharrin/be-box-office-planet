package fr.eql.ai110.bebop.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * Admin is a user with all functionalities available for use
 * @author Thibault 
 *
 */
@Entity
@Table(name="admin")
public class Admin implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//ATTRIBUTES
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "username")
	private String username;
	@Column(name = "password")
	private String password;
	@Column(name = "email_recovery")
	private String emailRecovery;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "create_time")
	private LocalDateTime createTime;
	@Column(name = "deactivation_time")
	private LocalDateTime deactivationTime;
	@Column(name = "permanently_delete")
	private LocalDateTime permanentlyDelete;

	//CONSTRUCTORS
	public Admin() {
		super();
	}

	public Admin(Integer id, String username, String password, String emailRecovery, String firstName, String lastName,
			LocalDateTime createTime, LocalDateTime deactivationTime, LocalDateTime permanentlyDelete) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.emailRecovery = emailRecovery;
		this.firstName = firstName;
		this.lastName = lastName;
		this.createTime = createTime;
		this.deactivationTime = deactivationTime;
		this.permanentlyDelete = permanentlyDelete;
	}

	//GETTERS & SETTERS
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailRecovery() {
		return emailRecovery;
	}

	public void setEmailRecovery(String emailRecovery) {
		this.emailRecovery = emailRecovery;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDateTime getCreateTime() {
		return createTime;
	}

	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	public LocalDateTime getDeactivationTime() {
		return deactivationTime;
	}

	public void setDeactivationTime(LocalDateTime deactivationTime) {
		this.deactivationTime = deactivationTime;
	}

	public LocalDateTime getPermanentlyDelete() {
		return permanentlyDelete;
	}

	public void setPermanentlyDelete(LocalDateTime permanentlyDelete) {
		this.permanentlyDelete = permanentlyDelete;
	}
}