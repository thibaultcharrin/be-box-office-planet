package fr.eql.ai110.bebop.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * Day is a day of the week to serve as reference for venues schedule
 * @author Thibault 
 *
 */
@Entity
@Table(name="day")
public class Day implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//ATTRIBUTES
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "name")
	private String name;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Schedule schedule;

	//CONSTRUCTORS
	public Day() {
		super();
	}

	public Day(Integer id, String name, Schedule schedule) {
		super();
		this.id = id;
		this.name = name;
		this.schedule = schedule;
	}

	//GETTERS & SETTERS
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	
}

