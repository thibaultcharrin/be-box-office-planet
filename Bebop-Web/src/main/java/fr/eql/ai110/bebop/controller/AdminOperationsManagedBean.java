package fr.eql.ai110.bebop.controller;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.eql.ai110.bebop.entity.Admin;
import fr.eql.ai110.bebop.entity.Client;
import fr.eql.ai110.bebop.entity.Partner;
import fr.eql.ai110.bebop.entity.Reward;
import fr.eql.ai110.bebop.entity.RewardType;
import fr.eql.ai110.bebop.ibusiness.AdminIBusiness;
import fr.eql.ai110.bebop.ibusiness.ClientIBusiness;
import fr.eql.ai110.bebop.ibusiness.PartnerIBusiness;
import fr.eql.ai110.bebop.ibusiness.RewardIBusiness;
import fr.eql.ai110.bebop.ibusiness.RewardTypeIBusiness;
import net.bootsfaces.utils.FacesMessages;

@ManagedBean(name="mbAdminOperations")
@SessionScoped
public class AdminOperationsManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private ClientIBusiness clientBusiness;
	@EJB
	private PartnerIBusiness partnerBusiness;
	@EJB
	private AdminIBusiness adminBusiness;
	@EJB
	private RewardIBusiness rewardBusiness;
	@EJB
	private RewardTypeIBusiness rewardTypeBusiness;



	// ***** ATTRIBUTES *****

	//Users

	private Admin admin;
	private List<Admin> admins;
	private Partner partner;
	private List<Partner> partners;
	private Client client;
	private List<Client> clients;

	private String adminUsername;
	private String password;
	private String emailRecovery;
	private String firstName;
	private String lastName;

	private String partnerUsername;
	private String clientUsername;

	//Rewards

	private RewardType rewardType;
	private List<RewardType> rewardTypes;
	private Reward reward;
	private List<Reward> rewards;

	private String rewardInfo;
	private String rewardCode;
	private Integer rewardTypeFK;
	private String rewardTypeDescription;
	private String rewardTypeName;
	private String rewardTypePicture;
	private Integer rewardTypePoints;

	//Messages
	
	private String adminMessage;
	private String partnerMessage;
	private String clientMessage;
	private String rewardTypeMessage;
	private String rewardMessage;


	private Integer idBuffer;	//Temporarily stores the Id of a Unique Selection, useful for a quick Update or Delete operation



	// ***** METHODS *****

	@PostConstruct
	public void init() {
		getAllAdmins();
		getAllPartners();
		getAllClients();
		getAllRewardTypes();
		getAllRewards();
	}

	public void getAllAdmins() {
		admins = adminBusiness.findAllAdmins();
	}
	public void getAllPartners() {
		partners = partnerBusiness.findAllPartners();
	}
	public void getAllClients() {
		clients = clientBusiness.findAllClients();
	}
	public void getAllRewardTypes() {
		rewardTypes = rewardTypeBusiness.findAllRewardTypes();
	}
	public void getAllRewards() {
		rewards = rewardBusiness.findAllRewards();
	}

	public void clearFields() {
		adminUsername = "";
		partnerUsername = "";
		password = "";
		emailRecovery = "";
		firstName = "";
		lastName = "";
		clientUsername = "";
		rewardInfo = "";
		rewardCode = "";
		rewardTypeFK = null;
		rewardTypeDescription = "";
		rewardTypeName = "";
		rewardTypePicture = "";
		rewardTypePoints = null;
		idBuffer = null;
	}

	public String createAdminMessage(boolean myCondition, Integer archetypeSize) {
		if (myCondition) {
			adminMessage = archetypeSize + " administrateur(s) trouvé(s)";
		} else {
			adminMessage = "Oups nous n'avons rien trouvé";
		}
		return adminMessage;
	}

	public String createPartnerMessage(boolean myCondition, Integer archetypeSize) {
		if (myCondition) {
			partnerMessage = archetypeSize + " partneraire(s) trouvé(s)";
		} else {
			partnerMessage = "Oups nous n'avons rien trouvé";
		}
		return clientMessage;
	}

	public String createClientMessage(boolean myCondition, Integer archetypeSize) {
		if (myCondition) {
			clientMessage = archetypeSize + " client(s) trouvé(s)";
		} else {
			clientMessage = "Oups nous n'avons rien trouvé";
		}
		return clientMessage;
	}

	public String createRewardTypeMessage(boolean myCondition, Integer archetypeSize) {
		if (myCondition) {
			rewardTypeMessage = archetypeSize + " type(s) de récompense(s) trouvé(s)";
		} else {
			rewardTypeMessage = "Oups nous n'avons rien trouvé";
		}
		return rewardTypeMessage;
	}

	public String createRewardMessage(boolean myCondition, Integer archetypeSize) {
		if (myCondition) {
			rewardMessage = archetypeSize + " récompense(s) trouvée(s)";
		} else {
			rewardMessage = "Oups nous n'avons rien trouvé";
		}
		return rewardMessage;
	}



	// *** CREATE ***

	//Admin

	public void createAdmin() {
		admin = new Admin();
		admin.setUsername(adminUsername);
		admin.setPassword(password);
		admin.setEmailRecovery(emailRecovery);
		admin.setFirstName(firstName);
		admin.setLastName(lastName);
		admin.setCreateTime(LocalDateTime.now());
		boolean verify = adminBusiness.createAdmin(admin);
		if (verify) {
			FacesMessages.info("Le compte " + adminUsername + " a bien été créé :) ");
			clearFields();
			searchAllAdminsByName();
		} else {
			FacesMessages.warning("Oups c'est déjà pris ou les champs ne sont pas respectés :O");
		}
	}

	//Rewards

	public void createRewardType() {
		rewardType = new RewardType();
		rewardType.setName(rewardTypeName);
		rewardType.setDescription(rewardTypeDescription);
		rewardType.setPicture(rewardTypePicture);
		rewardType.setPoints(rewardTypePoints);
		boolean verify = rewardTypeBusiness.createRewardType(rewardType);
		if (verify) {
			FacesMessages.info("Le type de récompense " + rewardTypeName + " a bien été créé :) ");
			clearFields();
			searchAllRewardTypesByName();
		} else {
			FacesMessages.warning("Oups c'est déjà pris ou les champs ne sont pas respectés :O");
		}
	}

	public void createReward() {
		reward = new Reward();
		reward.setInfo(rewardInfo);
		reward.setRedeemCode(rewardCode);
		reward.setRewardType(rewardTypeBusiness.findRewardTypeById(rewardTypeFK));
		boolean verify = rewardBusiness.createReward(reward);
		if (verify) {
			FacesMessages.info("La récompense " + rewardInfo + " a bien été créé :) ");
			clearFields();
			searchAllRewardsByName();
		} else {
			FacesMessages.warning("Oups c'est déjà pris ou les champs ne sont pas respectés :O");
		}
	}



	// *** READ ***

	//Admins
	public void searchAllAdminsByMulti() {
		admin = new Admin();
		admin.setUsername(adminUsername);
		admin.setEmailRecovery(emailRecovery);
		admin.setFirstName(firstName);
		admin.setLastName(lastName);
		admins = adminBusiness.findAdminsByMulti(admin);
		createAdminMessage(admins.size()>0,admins.size());
		if (admins.size()==1) {
			idBuffer = admins.get(0).getId();
			admin = adminBusiness.findAdminById(idBuffer);
			adminUsername = admin.getUsername();
			emailRecovery = admin.getEmailRecovery();
			firstName = admin.getFirstName();
			lastName = admin.getLastName();
		} else {
			clearFields();
		}
	}
	
	public void searchAllAdminsByName() {
		admins = adminBusiness.findAdminsByName(adminUsername);
		createAdminMessage(admins.size()>0,admins.size());
		if (admins.size()==1) {
			idBuffer = admins.get(0).getId();
			admin = adminBusiness.findAdminById(idBuffer);
			adminUsername = admin.getUsername();
			emailRecovery = admin.getEmailRecovery();
			firstName = admin.getFirstName();
			lastName = admin.getLastName();
		} else {
			clearFields();
		}
	}

	public void searchOneAdminById(Admin admin) {
		if (admins.size()!=1) {
			admins = new ArrayList<Admin>();
			admins.add(admin);
			idBuffer = admin.getId();
			adminUsername = admin.getUsername();
			emailRecovery = admin.getEmailRecovery();
			firstName = admin.getFirstName();
			lastName = admin.getLastName();
			this.admin = admin;
			createAdminMessage(admins.size()>0,admins.size());
		} else {
			clearFields();
			searchAllAdminsByName();
		}
	}

	//Partners
	
	public void searchAllPartnersByName() {
		partners = partnerBusiness.findPartnersByName(partnerUsername);
		createPartnerMessage(partners.size()>0,partners.size());
		if (partners.size()==1) {
			idBuffer = partners.get(0).getId();
			partner = partnerBusiness.findPartnerById(idBuffer);
		} else {
			clearFields();
		}
	}
	
	public void searchOnePartnerById(Partner partner) {
		if (partners.size()!=1) {
			partners = new ArrayList<Partner>();
			partners.add(partner);
			idBuffer = partner.getId();
			partnerUsername = partner.getUsername();
			this.partner = partner;
			createPartnerMessage(partners.size()>0,partners.size());
		} else {
			clearFields();
			searchAllPartnersByName();
		}
	}

	//Clients
	
	public void searchAllClientsByName() {
		clients = clientBusiness.findClientsByName(clientUsername);
		createClientMessage(clients.size()>0,clients.size());
		if (clients.size()==1) {
			idBuffer = clients.get(0).getId();
			client = clientBusiness.findClientById(idBuffer);
		} else {
			clearFields();
		}
	}
	
	public void searchOneClientById(Client client) {
		if (clients.size()!=1) {
			clients = new ArrayList<Client>();
			clients.add(client);
			idBuffer = client.getId();
			clientUsername = client.getUsername();
			this.client = client;
			createClientMessage(clients.size()>0,clients.size());
		} else {
			clearFields();
			searchAllClientsByName();
		}
	}

	//Reward Types

	public void searchAllRewardTypesByName() {
		rewardTypes = rewardTypeBusiness.findRewardTypesByName(rewardTypeName);
		createRewardTypeMessage(rewardTypes.size()>0,rewardTypes.size());
		if (rewardTypes.size()==1) {
			idBuffer = rewardTypes.get(0).getId();
			rewardType = rewardTypeBusiness.findRewardTypeById(idBuffer);
			rewardTypeName = rewardType.getName();
			rewardTypePoints = rewardType.getPoints();
			rewardTypeDescription = rewardType.getDescription();
			rewardTypePicture = rewardType.getPicture();
		} else {
			clearFields();
		}
	}
	
	public void searchOneRewardTypeById(RewardType rewardType) {
		if (rewardTypes.size()!=1) {
			rewardTypes = new ArrayList<RewardType>();
			rewardTypes.add(rewardType);
			idBuffer = rewardType.getId();
			rewardTypeName = rewardType.getName();
			rewardTypePoints = rewardType.getPoints();
			rewardTypeDescription = rewardType.getDescription();
			rewardTypePicture = rewardType.getPicture();
			this.rewardType = rewardType;
			createRewardTypeMessage(rewardTypes.size()>0,rewardTypes.size());
		} else {
			clearFields();
			searchAllRewardTypesByName();
		}
	}
	
	//Rewards

	public void searchAllRewardsByName() {
		rewards = rewardBusiness.findRewardsByName(rewardCode);
		createRewardMessage(rewards.size()>0,rewards.size());
		if (rewards.size()==1) {
			idBuffer = rewards.get(0).getId();
			reward = rewardBusiness.findRewardById(idBuffer);
			rewardInfo = reward.getInfo();
			rewardCode = reward.getRedeemCode();
		} else {
			clearFields();
		}
	}

	public void searchOneRewardById(Reward reward) {
		if (rewards.size()!=1) {
			rewards = new ArrayList<Reward>();
			rewards.add(reward);
			idBuffer = reward.getId();
			rewardInfo = reward.getInfo();
			rewardCode = reward.getRedeemCode();
			rewardTypeFK = reward.getRewardType().getId();	//ALLOWS SELECTOR TO BE IN THE RIGHT PLACE
			this.reward = reward;
			createRewardMessage(rewards.size()>0,rewards.size());
		} else {
			clearFields();
			searchAllRewardsByName();
		}
	}


	// *** UPDATE ***

	//Users

	public void modifyAdmin() {
		if (idBuffer!=null) {
			admin = adminBusiness.findAdminById(idBuffer);
			admin.setUsername(adminUsername);
			admin.setEmailRecovery(emailRecovery);
			admin.setFirstName(firstName);
			admin.setLastName(lastName);
			admin.setPassword(password);
			boolean verify = adminBusiness.updateAdmin(admin);
			if (verify) {
				admins = adminBusiness.findAdminsByName(adminUsername);
				FacesMessages.info("Votre compte " + adminUsername + " a bien été mis à jour");
			} else {
				FacesMessages.warning("Oups c'est déjà pris ou les champs ne sont pas respectés :O");
			}
		} else {
			searchAllAdminsByName();
			adminMessage = "veuillez choisir un seul utilisateur";
		}
	}

	public void validatePartner() {
		if (idBuffer!=null) {
			partner = partnerBusiness.findPartnerById(idBuffer);
			if (partner.getAdminApproval()==null) {
				partner.setAdminApproval(LocalDateTime.now());
				partnerBusiness.updatePartner(partner);
				partners = partnerBusiness.findPartnersByName(partner.getUsername());
				FacesMessages.info(partner.getUsername() + " est désormais validé partenaire"); 
			} else {
				FacesMessages.warning(partner.getUsername() + " est déjà partenaire");
			}
		} else {
			searchAllPartnersByName();
			partnerMessage = "veuillez choisir un seul utilisateur";
		}
	}

	//Rewards

	public void modifyRewardType() {
		if (idBuffer!=null) {
			rewardType = rewardTypeBusiness.findRewardTypeById(idBuffer);
			rewardType.setName(rewardTypeName);
			rewardType.setPoints(rewardTypePoints);
			rewardType.setDescription(rewardTypeDescription);
			rewardType.setPicture(rewardTypePicture);
			boolean verify = rewardTypeBusiness.updateRewardType(rewardType);
			if (verify) {
				rewardTypes = rewardTypeBusiness.findRewardTypesByName(rewardTypeName);
				FacesMessages.info("Votre type de récompense " + rewardTypeName + " a bien été mise à jour");
			} else {
				FacesMessages.warning("Oups c'est déjà pris ou les champs ne sont pas respectés :O");
			}
		} else {
			searchAllRewardTypesByName();
			FacesMessages.warning("veuillez choisir un seul type de récompense");
		}
	}

	public void modifyReward() {
		if (idBuffer!=null) {
			reward = rewardBusiness.findRewardById(idBuffer);
			reward.setInfo(rewardInfo);
			reward.setRedeemCode(rewardCode);
			reward.setRewardType(rewardTypeBusiness.findRewardTypeById(rewardTypeFK));
			boolean verify = rewardBusiness.updateReward(reward);
			if (verify) {
				rewards = rewardBusiness.findRewardsByName(rewardCode);
				FacesMessages.info("Votre récompense " + rewardInfo + " a bien été mise à jour");
			} else {
				FacesMessages.warning("Oups c'est déjà pris ou les champs ne sont pas respectés :O");
			}
		} else {
			searchAllRewardsByName();
			FacesMessages.warning("veuillez choisir une seule récompense");
		}
	}



	// *** DELETE ***

	// Users

	public void removeAdmin() {
		if (idBuffer!=null) {
			admin = adminBusiness.findAdminById(idBuffer);
			boolean verify = adminBusiness.deleteAdmin(admin);
			if (verify) {
				FacesMessages.info("Cet administrateur a bien été supprimé");
				clearFields();
				searchAllAdminsByName();
			} else {
				admin = adminBusiness.findAdminById(idBuffer);
				FacesMessages.warning("Cet administrateur ne peut pas être supprimé");
			}
		} else {
			searchAllAdminsByName();
			FacesMessages.warning("Veuillez sélectionner un seul administrateur");
		}
	}

	public void removePartner() {
		if (idBuffer!=null) {
			partner = partnerBusiness.findPartnerById(idBuffer);
			boolean verify = partnerBusiness.deletePartner(partner);
			if (verify) {
				FacesMessages.info("Ce partneraire a bien été supprimé");
				clearFields();
				searchAllPartnersByName();
			} else {
				FacesMessages.warning("Ce partneraire ne peut pas être supprimé");
			}
		} else {
			searchAllPartnersByName();
			FacesMessages.warning("Veuillez sélectionner un seul partenaire");
		}
	}

	public void removeClient() {
		if (idBuffer!=null) {
			client = clientBusiness.findClientById(idBuffer);
			boolean verify = clientBusiness.deleteClient(client);
			if (verify) {
				FacesMessages.info("Ce client a bien été supprimé");
				clearFields();
				searchAllClientsByName();
			} else {
				FacesMessages.warning("Ce client ne peut pas être supprimé");
			}
		} else {
			searchAllClientsByName();
			FacesMessages.warning("Veuillez sélectionner un seul client");
		}
	}

	//Rewards 

	public void removeRewardType() {
		if (idBuffer!=null) {
			rewardType = rewardTypeBusiness.findRewardTypeById(idBuffer);
			boolean verify = rewardTypeBusiness.deleteRewardType(rewardType);
			if (verify) {
				FacesMessages.info("Ce type de récompense a bien été supprimé");
				clearFields();
				searchAllRewardTypesByName();
			} else {
				FacesMessages.warning("Ce type de récompense n'a pas été supprimé");
			}
		} else {
			searchAllRewardTypesByName();
			FacesMessages.warning("Veuillez sélectionner un seul type de récompense");
		}
	}

	public void removeReward() {
		if (idBuffer!=null) {
			reward = rewardBusiness.findRewardById(idBuffer);
			boolean verify = rewardBusiness.deleteReward(reward);
			if (verify) {
				FacesMessages.info("Cette récompense a bien été supprimé");
				clearFields();
				searchAllRewardsByName();
			} else {
				FacesMessages.info("Cette récompense n'a pas été supprimé");
			}
		} else {
			searchAllRewardsByName();
			FacesMessages.info("Veuillez sélectionner une seule récompense");
		}
	}
	
	

	// ***** GETTERS & SETTERS *****

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public List<Admin> getAdmins() {
		return admins;
	}

	public void setAdmins(List<Admin> admins) {
		this.admins = admins;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public List<Partner> getPartners() {
		return partners;
	}

	public void setPartners(List<Partner> partners) {
		this.partners = partners;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public String getAdminUsername() {
		return adminUsername;
	}

	public void setAdminUsername(String adminUsername) {
		this.adminUsername = adminUsername;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailRecovery() {
		return emailRecovery;
	}

	public void setEmailRecovery(String emailRecovery) {
		this.emailRecovery = emailRecovery;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPartnerUsername() {
		return partnerUsername;
	}

	public void setPartnerUsername(String partnerUsername) {
		this.partnerUsername = partnerUsername;
	}

	public String getClientUsername() {
		return clientUsername;
	}

	public void setClientUsername(String clientUsername) {
		this.clientUsername = clientUsername;
	}

	public RewardType getRewardType() {
		return rewardType;
	}

	public void setRewardType(RewardType rewardType) {
		this.rewardType = rewardType;
	}

	public List<RewardType> getRewardTypes() {
		return rewardTypes;
	}

	public void setRewardTypes(List<RewardType> rewardTypes) {
		this.rewardTypes = rewardTypes;
	}

	public Reward getReward() {
		return reward;
	}

	public void setReward(Reward reward) {
		this.reward = reward;
	}

	public List<Reward> getRewards() {
		return rewards;
	}

	public void setRewards(List<Reward> rewards) {
		this.rewards = rewards;
	}

	public String getRewardInfo() {
		return rewardInfo;
	}

	public void setRewardInfo(String rewardInfo) {
		this.rewardInfo = rewardInfo;
	}

	public String getRewardCode() {
		return rewardCode;
	}

	public void setRewardCode(String rewardCode) {
		this.rewardCode = rewardCode;
	}

	public Integer getRewardTypeFK() {
		return rewardTypeFK;
	}

	public void setRewardTypeFK(Integer rewardTypeFK) {
		this.rewardTypeFK = rewardTypeFK;
	}

	public String getRewardTypeDescription() {
		return rewardTypeDescription;
	}

	public void setRewardTypeDescription(String rewardTypeDescription) {
		this.rewardTypeDescription = rewardTypeDescription;
	}

	public String getRewardTypeName() {
		return rewardTypeName;
	}

	public void setRewardTypeName(String rewardTypeName) {
		this.rewardTypeName = rewardTypeName;
	}

	public String getRewardTypePicture() {
		return rewardTypePicture;
	}

	public void setRewardTypePicture(String rewardTypePicture) {
		this.rewardTypePicture = rewardTypePicture;
	}

	public Integer getRewardTypePoints() {
		return rewardTypePoints;
	}

	public void setRewardTypePoints(Integer rewardTypePoints) {
		this.rewardTypePoints = rewardTypePoints;
	}

	public String getAdminMessage() {
		return adminMessage;
	}

	public void setAdminMessage(String adminMessage) {
		this.adminMessage = adminMessage;
	}

	public String getPartnerMessage() {
		return partnerMessage;
	}

	public void setPartnerMessage(String partnerMessage) {
		this.partnerMessage = partnerMessage;
	}

	public String getClientMessage() {
		return clientMessage;
	}

	public void setClientMessage(String clientMessage) {
		this.clientMessage = clientMessage;
	}

	public String getRewardTypeMessage() {
		return rewardTypeMessage;
	}

	public void setRewardTypeMessage(String rewardTypeMessage) {
		this.rewardTypeMessage = rewardTypeMessage;
	}

	public String getRewardMessage() {
		return rewardMessage;
	}

	public void setRewardMessage(String rewardMessage) {
		this.rewardMessage = rewardMessage;
	}

	public Integer getIdBuffer() {
		return idBuffer;
	}

	public void setIdBuffer(Integer idBuffer) {
		this.idBuffer = idBuffer;
	}
}
