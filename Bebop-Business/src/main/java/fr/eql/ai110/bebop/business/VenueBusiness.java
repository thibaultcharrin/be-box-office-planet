package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.bebop.entity.Venue;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

import fr.eql.ai110.bebop.ibusiness.VenueIBusiness;
import fr.eql.ai110.bebop.idao.VenueIDao;

@Remote(VenueIBusiness.class)
@Stateless
public class VenueBusiness implements VenueIBusiness {

	@EJB
	private VenueIDao venueDao;

	//CREATE
	@Override
	public boolean createVenue(Venue venue) {
		String myName = venue.getName();
		boolean verify = venueDao.isNameAvailable(myName);
		if (verify) venueDao.add(venue);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//READ
	@Override
	public List<Venue> findAllVenues() {
		//TODO getAll control layer
		return venueDao.getAll();
	}

	@Override
	public List<Venue> findVenuesByName(String name) {
		// TODO getArchetypeEntitiesByName control layer
		return venueDao.getVenuesByName(name);
	}

	@Override
	public Venue findVenueById(Integer id) {
		// TODO getById control layer
		return venueDao.getById(id);
	}

	@Override
	public Long findNbVenues() {
		Long result = venueDao.countVenues();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateVenue(Venue venue) {
		String myName = venue.getName();
		boolean verify = venueDao.isNameAvailable(myName);
		if (verify) venueDao.update(venue);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//DELETE
	@Override
	public boolean deleteVenue(Venue venue) {
		int id = venue.getId();
		boolean verify = venueDao.getById(id)!=null;
		if (verify) venueDao.delete(venue);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
