package fr.eql.ai110.bebop.business;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;


/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

import fr.eql.ai110.bebop.entity.Client;
import fr.eql.ai110.bebop.ibusiness.ClientIBusiness;
import fr.eql.ai110.bebop.idao.AdminIDao;
import fr.eql.ai110.bebop.idao.ClientIDao;
import fr.eql.ai110.bebop.idao.PartnerIDao;

@Remote(ClientIBusiness.class)
@Stateless
public class ClientBusiness implements ClientIBusiness {

	@EJB
	private ClientIDao clientDao;
	
	@EJB
	private AdminIDao adminDao;
	
	@EJB
	private PartnerIDao partnerDao;

	private final static Integer MIN_LENGTH = 2;
	private final static Integer MAX_LENGTH = 30;

	//AUTHENTICATION
	@Override
	public String hashPassword(String password) {
		String hashedPassword = "";
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
			hashedPassword = new String(encodedhash, StandardCharsets.UTF_8);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hashedPassword;
	}

	@Override
	public Client connectClient(Client client) {
		String username = client.getUsername();
		String password = hashPassword(client.getPassword());
		return clientDao.clientAuthenticate(username, password);
	}

	//CREATE
	@Override
	public boolean createClient(Client client) {
		String myName = client.getUsername();
		String myPassword = client.getPassword();
		boolean verify = myName.length()>MIN_LENGTH
				&&myName.length()<MAX_LENGTH
				&&myPassword.length()>MIN_LENGTH
				&&myPassword.length()<MAX_LENGTH
				&&clientDao.isNameAvailable(myName)
				&&adminDao.isNameAvailable(myName)
				&&partnerDao.isNameAvailable(myName);
		if (verify) {
			client.setPassword(hashPassword(myPassword));
			clientDao.add(client);
		}
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//READ
	@Override
	public List<Client> findAllClients() {
		return clientDao.getAll();
	}

	@Override
	public List<Client> findClientsByMulti(Client client) {
		String username = client.getUsername();
		String email = client.getEmail();
		String firstName = client.getFirstName();
		String lastName = client.getLastName();
		return clientDao.getClientsByMulti(username, email, firstName, lastName);
	}

	@Override
	public List<Client> findClientsByName(String name) {
		return clientDao.getClientsByName(name);
	}

	@Override
	public Client findClientById(Integer id) {
		return clientDao.getById(id);
	}

	@Override
	public Long findNbClients() {
		Long result = clientDao.countClients();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateClient(Client client) {
		boolean newUsernameVerify = true;
		boolean newPasswordVerify = true;
		
		String oldUsername = clientDao.getById(client.getId()).getUsername();
		String newUsername = client.getUsername();
		String oldPassword = clientDao.getById(client.getId()).getPassword();
		String newPassword = client.getPassword();

		if (!oldUsername.equals(newUsername)) {
			newUsernameVerify = newUsername.length()>MIN_LENGTH
					&&newUsername.length()<MAX_LENGTH
					&&partnerDao.isNameAvailable(newUsername)
					&&adminDao.isNameAvailable(newUsername)
					&&clientDao.isNameAvailable(newUsername);
		}

		if (!oldPassword.equals(newPassword)) {
			newPasswordVerify = newPassword.length()>MIN_LENGTH
					&&newPassword.length()<MAX_LENGTH;
			client.setPassword(hashPassword(newPassword));
		}
		
		boolean allConditionsVerified = newUsernameVerify&&newPasswordVerify;
		
		if (allConditionsVerified) {
			clientDao.update(client);
		}
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return allConditionsVerified;
	}

	//DELETE (Updates fields to null)
	@Override
	public boolean deleteClient(Client client) {
		boolean verify = client.getUsername()!=null
				&&client.getPermanentlyDelete()!=null;
		if (verify) {
			client.setBirthdate(null);
			client.setCreateTime(null);
			client.setDeactivationTime(null);
			client.setEmail(null);
			client.setFirstName(null);
			client.setLastName(null);
			client.setPassword(null);
			client.setPhone(null);
			client.setPoints(null);
			client.setUsername(null);
			client.setValidationCode(null);
			client.setValidationTime(null);
			clientDao.update(client);
		} 
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
