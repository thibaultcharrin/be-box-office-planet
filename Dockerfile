# Multi-stage build
# Maven JDK 8
FROM maven:3.9.4-amazoncorretto-8-debian-bookworm as build
WORKDIR /usr/src/app
RUN apt-get update -y && apt-get install -y curl
RUN curl -LO https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-j-8.0.33.tar.gz
RUN tar --strip-components=1 -xzvf mysql-connector-j-8.0.33.tar.gz mysql-connector-j-8.0.33/mysql-connector-j-8.0.33.jar
COPY . .
RUN mvn clean install -T 1C

# WildFly Server
FROM quay.io/wildfly/wildfly:26.1.2.Final
COPY --from=build /usr/src/app/Bebop-Ear/target/*.ear /opt/jboss/wildfly/standalone/deployments/
COPY --from=build /usr/src/app/*.jar /opt/jboss/wildfly/modules/system/layers/base/com/mysql/main/
COPY standalone.xml /opt/jboss/wildfly/standalone/configuration/
COPY module.xml /opt/jboss/wildfly/modules/system/layers/base/com/mysql/main/

RUN /opt/jboss/wildfly/bin/add-user.sh root root --silent
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]

# Resources
# https://www.wildfly.org/news/2022/08/31/WildFly2612-Released/
# https://quay.io/repository/wildfly/wildfly