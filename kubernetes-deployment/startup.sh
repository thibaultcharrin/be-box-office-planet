#!/bin/bash

#DOCKER
IMAGE=thibaultcharrin/be-box-office-planet
TAG=latest
CONTAINER_REGISTRY="" # "" for Docker Hub; "registry.gitlab.com/" for GitLab
#KUBERNETES
CPUS=""               # "--cpus 2"
MEMORY=""             # "--memory 4096"
KUBERNETES_VERSION="" # "--kubernetes-version v1.26.1"
KUBE_SECRET=""
MYSQL_ROOT_PASSWORD=$(grep MYSQL_ROOT_PASSWORD ../.env | cut -d "=" -f 2 | tr -d '\n' | base64 -w0)
MYSQL_DATABASE=$(grep MYSQL_DATABASE ../.env | cut -d "=" -f 2 | tr -d '\n' | base64 -w0)
MYSQL_USER=$(grep MYSQL_USER ../.env | cut -d "=" -f 2 | tr -d '\n' | base64 -w0)
MYSQL_PASSWORD=$(grep MYSQL_PASSWORD ../.env | cut -d "=" -f 2 | tr -d '\n' | base64 -w0)

function check_prerequisites() {
    echo -e "\n*** Checking Prerequisites ***"
    PREREQUISITES=(docker kubectl minikube)
    for PREREQUISITE in "${PREREQUISITES[@]}"; do
        if [[ ! $(command -v "$PREREQUISITE") ]]; then
            echo "Please install $PREREQUISITE prior to launching this script"
            exit 1
        fi
    done
    echo "You are all set! "
}

function check_docker_login() {
    echo -e "\n*** Checking Docker Login ***"
    if [[ -f ~/.docker/config.json ]]; then
        if [[ $CONTAINER_REGISTRY == "registry.gitlab.com/" ]]; then
            if [[ "$(grep -c registry.gitlab.com ~/.docker/config.json)" != 0 ]]; then
                echo "The present config.json will be used for accessing the GitLab Registry"
            else
                echo "please enter docker login registry.gitlab.com command prior to launching pipeline"
                exit 1
            fi
        else echo "You are all set! "; fi
        KUBE_SECRET=$(cat ~/.docker/config.json | base64 -w0)
    else
        echo "please configure a config.json file with the docker login command"
        exit 1
    fi
}

function check_kubernetes() {
    echo -e "\n*** Checking Kubernetes ***"
    if [[ "$(kubectl config get-contexts --no-headers | wc -l)" == 0 ]]; then
        minikube start $CPUS $MEMORY $KUBERNETES_VERSION
        minikube addons enable dashboard
        minikube addons enable metrics-server
        minikube addons enable ingress
        minikube profile list
        minikube addons list
    elif [ "$(kubectl config get-contexts --no-headers | wc -l)" -gt 1 ]; then
        echo -e "\nPlease choose one of the following clusters: "
        CURRENT=$(kubectl config current-context)
        kubectl config get-contexts
        read -rp "Cluster name? (default: ${CURRENT}) " CONTEXT
        if [[ $CONTEXT != "" ]]; then kubectl config use-context "$CONTEXT"; fi
    fi
    kubectl config current-context
    kubectl cluster-info
}

function prepare_descriptors() {
    echo -e "\n*** Preparing Descriptors ***"
    DESTINATION=$CONTAINER_REGISTRY$IMAGE:$TAG
    cp 2-secrets.yaml 2-secrets-temp.yaml
    cp 5-app.yaml 5-app-temp.yaml
    sed -i -e "s|\${KUBE_SECRET}|$KUBE_SECRET|g" \
        -e "s|\${MYSQL_ROOT_PASSWORD}|$MYSQL_ROOT_PASSWORD|g" \
        -e "s|\${MYSQL_DATABASE}|$MYSQL_DATABASE|g" \
        -e "s|\${MYSQL_USER}|$MYSQL_USER|g" \
        -e "s|\${MYSQL_PASSWORD}|$MYSQL_PASSWORD|g" 2-secrets-temp.yaml
    sed -i "s|\${DESTINATION}|$DESTINATION|g" 5-app-temp.yaml
}

function deploying_app() {
    echo -e "\n*** Deploying App ***"
    kubectl apply -f 1-namespace.yaml -f 2-secrets-temp.yaml -f 3-volume.yaml -f 4-mysql.yaml
    while [[ "$(kubectl rollout status deploy bebop-mysql -n yellow-submarine | grep -c success)" == 0 ]]; do # kubectl rollout status awaits "successfully rolled out"
        sleep 1
    done
    kubectl apply -f 5-app-temp.yaml -f 6-ingress.yaml
}

function final_steps() {
    echo -e "\n*** Final steps ***"
    rm ./*temp.yaml
    kubectl get namespaces
}

function main() {
    case $1 in
    "--TAG")
        if [[ "$2" != "" ]]; then
            TAG="$2"
        else echo "Usage: ./startup --TAG [TAG]"; fi
        ;;
    "--REG")
        if [[ "$2" != "" ]]; then
            CONTAINER_REGISTRY="$2"
        else echo "Usage:./startup --REG [CONTAINER_REGISTRY]"; fi
        ;;
    *) ;;
    esac
    check_prerequisites
    check_docker_login
    check_kubernetes
    prepare_descriptors
    deploying_app
    final_steps
}

main "$@"
