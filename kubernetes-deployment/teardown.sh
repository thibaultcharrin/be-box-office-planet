#!/bin/bash

#DOCKER
IMAGE=thibaultcharrin/be-box-office-planet
TAG=latest
CONTAINER_REGISTRY="" # "" for Docker Hub; "registry.gitlab.com/" for GitLab
#KUBERNETES
KUBE_SECRET=""
MYSQL_ROOT_PASSWORD=$(grep MYSQL_ROOT_PASSWORD ../.env | cut -d "=" -f 2 | tr -d '\n' | base64 -w0)
MYSQL_DATABASE=$(grep MYSQL_DATABASE ../.env | cut -d "=" -f 2 | tr -d '\n' | base64 -w0)
MYSQL_USER=$(grep MYSQL_USER ../.env | cut -d "=" -f 2 | tr -d '\n' | base64 -w0)
MYSQL_PASSWORD=$(grep MYSQL_PASSWORD ../.env | cut -d "=" -f 2 | tr -d '\n' | base64 -w0)

function prepare_descriptors() {
    echo -e "\n*** Preparing Descriptors ***"
    DESTINATION=$CONTAINER_REGISTRY$IMAGE:$TAG
    cp 2-secrets.yaml 2-secrets-temp.yaml
    cp 5-app.yaml 5-app-temp.yaml
    sed -i -e "s|\${KUBE_SECRET}|$KUBE_SECRET|g" \
        -e "s|\${MYSQL_ROOT_PASSWORD}|$MYSQL_ROOT_PASSWORD|g" \
        -e "s|\${MYSQL_DATABASE}|$MYSQL_DATABASE|g" \
        -e "s|\${MYSQL_USER}|$MYSQL_USER|g" \
        -e "s|\${MYSQL_PASSWORD}|$MYSQL_PASSWORD|g" 2-secrets-temp.yaml
    sed -i "s|\${DESTINATION}|$DESTINATION|g" 5-app-temp.yaml
}

function delete_resources() {
    echo -e "\n*** Deleting Resources ***"
    kubectl delete -f 2-secrets-temp.yaml -f 3-volume.yaml -f 4-mysql.yaml -f 5-app-temp.yaml -f 6-ingress.yaml
}

function final_steps() {
    echo -e "\n*** Final steps ***"
    rm ./*temp.yaml
    kubectl get namespaces
}

prepare_descriptors
delete_resources
final_steps
