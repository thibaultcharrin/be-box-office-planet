package fr.eql.ai110.bebop.idao;

import java.util.List;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.Ticket;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface TicketIDao extends GenericIDao<Ticket> {

	/** returns all tickets that contain these characters in name */
	List<Ticket> getTicketsByName(String name);
	
	/** returns the total count of tickets */
	Long countTickets();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);
	
}
