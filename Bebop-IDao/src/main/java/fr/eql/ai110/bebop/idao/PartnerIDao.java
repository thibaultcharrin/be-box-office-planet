package fr.eql.ai110.bebop.idao;

import java.util.List;

import fr.eql.ai110.bebop.entity.Partner;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface PartnerIDao extends GenericIDao<Partner> {

	/** returns all partners that contain these characters in name */
	List<Partner> getPartnersByName(String name);
	
	/** returns all partners by multicriteria **/
	List<Partner> getPartnersByMulti(String username, String email, String firstName, String lastName);
	
	/** returns the total count of partners */
	Long countPartners();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);

	/** authenticates partner **/
	Partner partnerAuthenticate(String username, String password);
}
