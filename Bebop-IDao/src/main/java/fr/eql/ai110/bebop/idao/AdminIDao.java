package fr.eql.ai110.bebop.idao;

import java.util.List;

import fr.eql.ai110.bebop.entity.Admin;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface AdminIDao extends GenericIDao<Admin> {

	/** returns all admins that contain these characters in name */
	List<Admin> getAdminsByName(String name);
	
	/** returns all admins by multicriteria */
	List<Admin> getAdminsByMulti(String username, String email, String firstname, String lastname);
	
	/** returns the total count of admins */
	Long countAdmins();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);

	/** authenticates admin **/
	Admin adminAuthenticate(String login, String password);
	
}
