package fr.eql.ai110.bebop.idao;

import java.util.List;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.Event;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface EventIDao extends GenericIDao<Event> {

	/** returns all events that contain these characters in name */
	List<Event> getEventsByName(String name);
	
	/** returns the total count of events */
	Long countEvents();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);
	
}
