package fr.eql.ai110.bebop.idao;

import java.util.List;

import fr.eql.ai110.bebop.entity.Client;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface ClientIDao extends GenericIDao<Client> {

	/** returns all clients that contain these characters in name */
	List<Client> getClientsByName(String name);
	
	/** returns the total count of clients */
	Long countClients();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);

	/** returns all clients by multicriteria **/
	List<Client> getClientsByMulti(String username, String email, String firstName, String lastName);

	/** Authenticates client **/
	Client clientAuthenticate(String username, String password);
	
}
