package fr.eql.ai110.bebop.idao;

import java.util.List;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.Subset;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface SubsetIDao extends GenericIDao<Subset> {

	/** returns all archetype entities that contain these characters in name */
	List<Subset> getSubsetsByName(String name);
	
	/** returns the total count of subsets */
	Long countSubsets();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);
	
}
